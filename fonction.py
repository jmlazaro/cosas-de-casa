# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
class Fonction:
    def __init__(self,ff,gradff,hessff,nom,n,m):
        self.val = ff
        self.grad = gradff
        self.hess = hessff
        self.nom = nom
        self.n = n
        self.m = m

    def _fplot(self,x1,x2,i):
        x=np.array([x1,x2])
        return self.val(x,i)

    def plotCont(self,xmin,xmax,ymin,ymax,ech):
        if self.n == 2 :
            X1 = np.arange(xmin,xmax,0.01)
            X2 = np.arange(ymin,ymax,0.01)
            X1, X2 = np.meshgrid(X1, X2)
            for i in range(1,self.m + 1):
                Z = self._fplot(X1, X2,i)
                plt.axis('equal')
                CS = plt.contour(X1, X2, Z, ech)
                plt.clabel(CS, inline=1, fontsize=8)
            plt.xlabel("x1")
            plt.ylabel("x2")
            titre="Contours de la fonction "+self.nom
            plt.title(titre)
        else:
            print("contour uniquement pour n=2")
